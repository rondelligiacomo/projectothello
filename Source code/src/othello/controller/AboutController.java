/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othello.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;

/**
 *  Class for program's "about" section with corresponding info.
 *
 *  @author Alberto Zini
 *  @author Giacomo Rondelli 
 *  @author Giacomo Pazzaglia
 *  @version 1.0 
 *  @since 2017.08.01
 */
public class AboutController implements Initializable {


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void exitStage(ActionEvent event) {
        ((Node)(event.getSource())).getScene().getWindow().hide();
    }
}
