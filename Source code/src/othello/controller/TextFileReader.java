package othello.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;

/**
 * This class allows the users to read the file called "fileLog", situated in path: "othello/model/fileLog.txt".
 * The file is written at the end of the game.
 * It will be shown in a new FXML document.
 *  @author Alberto Zini
 *  @author Giacomo Rondelli
 *  @author Giacomo Pazzaglia
 *  @version 1.0
 *  @since 2017.08.01
 */
public class TextFileReader {
   
   @SuppressWarnings("NestedAssignment")
   public List<String> read(File file) {
      List<String> lines = new ArrayList<>();
      String line;
      try {
         
         BufferedReader br = new BufferedReader(new FileReader(new File("fileLog.txt")));
         
         while ((line = br.readLine()) != null) {
            lines.add(line);
         }
         br.close();
      }
      catch(FileNotFoundException e){
         Alert alert = new Alert(Alert.AlertType.INFORMATION);
         alert.setHeaderText("Information!");
         alert.setTitle("Start game before");
         alert.setContentText("You have to click the start button before!");
         alert.showAndWait();
         
      } 
      catch (IOException ex) {
         Logger.getLogger(TextFileReader.class.getName()).log(Level.SEVERE, null, ex);
      }
      
      return lines;
   }
}