/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package othello.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.File;
import java.io.PrintWriter;
import javafx.scene.control.Alert;
import javafx.scene.media.AudioClip;
import static javafx.scene.media.AudioClip.INDEFINITE;

/**
 *  This class allows us to execute the game, with the corresponding moves in the challenge between the players.
 *
 *  @author Alberto Zini
 *  @author Giacomo Rondelli
 *  @author Giacomo Pazzaglia
 *  @version 1.0
 *  @since 2017.08.01
 */
public class BoardController implements Initializable {
   
   @FXML
   private Label label_count_p1;
   @FXML
   private Label label_count_p2;
   private Image black;
   private Image white;
   private Image empty;
   private Image marked;
   @FXML
   private GridPane myGrid;
   @FXML
   private Button btn_start;
   @FXML
   private RadioButton rd_p1_black;
   @FXML
   private RadioButton rd_p2_black;
   @FXML
   private RadioButton rd_p1_white;
   @FXML
   private RadioButton rd_p2_white;
   @FXML
   private ToggleGroup group_p1;
   @FXML
   private ToggleGroup group_p2;
   @FXML
   private Label label_output;
   @FXML
   private ImageView imgCount_p1;
   @FXML
   private ImageView imgCount_p2;
   private int cont_p1;
   private int cont_p2;
   @FXML
   private TextField txt_field_p1;
   @FXML
   private TextField txt_field_p2;
   private int cont;
   private boolean isFinita;
   @FXML
   private Label label_coord;
   @FXML
   private ImageView imageView00;
   @FXML
   private ImageView imageView01;
   @FXML
   private ImageView imageView02;
   @FXML
   private ImageView imageView03;
   @FXML
   private ImageView imageView04;
   @FXML
   private ImageView imageView05;
   @FXML
   private ImageView imageView06;
   @FXML
   private ImageView imageView07;
   @FXML
   private ImageView imageView10;
   @FXML
   private ImageView imageView11;
   @FXML
   private ImageView imageView12;
   @FXML
   private ImageView imageView13;
   @FXML
   private ImageView imageView14;
   @FXML
   private ImageView imageView15;
   @FXML
   private ImageView imageView16;
   @FXML
   private ImageView imageView17;
   @FXML
   private ImageView imageView20;
   @FXML
   private ImageView imageView21;
   @FXML
   private ImageView imageView22;
   @FXML
   private ImageView imageView23;
   @FXML
   private ImageView imageView24;
   @FXML
   private ImageView imageView25;
   @FXML
   private ImageView imageView26;
   @FXML
   private ImageView imageView27;
   @FXML
   private ImageView imageView30;
   @FXML
   private ImageView imageView31;
   @FXML
   private ImageView imageView32;
   @FXML
   private ImageView imageView33;
   @FXML
   private ImageView imageView34;
   @FXML
   private ImageView imageView35;
   @FXML
   private ImageView imageView36;
   @FXML
   private ImageView imageView37;
   @FXML
   private ImageView imageView40;
   @FXML
   private ImageView imageView41;
   @FXML
   private ImageView imageView42;
   @FXML
   private ImageView imageView43;
   @FXML
   private ImageView imageView44;
   @FXML
   private ImageView imageView45;
   @FXML
   private ImageView imageView46;
   @FXML
   private ImageView imageView47;
   @FXML
   private ImageView imageView50;
   @FXML
   private ImageView imageView51;
   @FXML
   private ImageView imageView52;
   @FXML
   private ImageView imageView53;
   @FXML
   private ImageView imageView54;
   @FXML
   private ImageView imageView55;
   @FXML
   private ImageView imageView56;
   @FXML
   private ImageView imageView57;
   @FXML
   private ImageView imageView60;
   @FXML
   private ImageView imageView61;
   @FXML
   private ImageView imageView62;
   @FXML
   private ImageView imageView63;
   @FXML
   private ImageView imageView64;
   @FXML
   private ImageView imageView65;
   @FXML
   private ImageView imageView66;
   @FXML
   private ImageView imageView67;
   @FXML
   private ImageView imageView70;
   @FXML
   private ImageView imageView71;
   @FXML
   private ImageView imageView72;
   @FXML
   private ImageView imageView73;
   @FXML
   private ImageView imageView74;
   @FXML
   private ImageView imageView75;
   @FXML
   private ImageView imageView76;
   @FXML
   private ImageView imageView77;
   private ArrayList<ImageView> myBoard;
   @FXML
   private Label p1_totScore;
   @FXML
   private Label p2_totScore;
   private int totScoreP1;
   private int totScoreP2;
   private ArrayList<String> lastPlays;
   @FXML
   private Button btn_pass;
   Alert alert;
   boolean check;
   boolean finishComplete;
   
   
   AudioClip audioLight = new AudioClip(getClass().getClassLoader().getResource("othello/music/background_music.wav").toExternalForm());
   
   AudioClip audioDark = new AudioClip(getClass().getClassLoader().getResource("othello/music/darktheme.wav").toExternalForm());
   
   AudioClip audioWild = new AudioClip(getClass().getClassLoader().getResource("othello/music/wild_battle.wav").toExternalForm());
   
   AudioClip audioFall = new AudioClip(getClass().getClassLoader().getResource("othello/music/heavens_fall.wav").toExternalForm());
   
   /**
    * Method to start music from menu
    */
    @FXML
   public void audioLightOn(){
      audioLight.stop();
      audioDark.stop();
      audioWild.stop();
      audioFall.stop();
      
      audioLight.setVolume(0.2);
      audioLight.setCycleCount(INDEFINITE);
      audioLight.play();
   }
   @FXML
   public void audioDarkOn(){
      audioLight.stop();
      audioDark.stop();
      audioWild.stop();
      audioFall.stop();
      
      audioDark.setVolume(0.2);
      audioDark.setCycleCount(INDEFINITE);
      audioDark.play();
   }
   
   @FXML
   private void audioWildOn(ActionEvent event) {
      audioLight.stop();
      audioDark.stop();
      audioWild.stop();
      audioFall.stop();
      
      audioWild.setVolume(0.2);
      audioWild.setCycleCount(INDEFINITE);
      audioWild.play();
   }
   
   @FXML
   private void audioFallOn(ActionEvent event) {
      audioLight.stop();
      audioDark.stop();
      audioWild.stop();
      audioFall.stop();
      
      audioFall.setVolume(0.1);
      audioFall.setCycleCount(INDEFINITE);
      audioFall.play();
   }
   
   /**
    * Method to disable music from menu
    */
   @FXML
   public void audioN(ActionEvent e)throws IOException{
      audioLight.stop();
      audioDark.stop();
      audioWild.stop();
      audioFall.stop();
   }
   
   /**
   	* Initialize the program disabling the grid, until boot requirements are properly set.
    */
   @Override
   public void initialize(URL url, ResourceBundle rb) {
      createMyBoard();
      lastPlays = new ArrayList<>();
      black = new Image("othello/images/black.png");
      white = new Image("othello/images/white.png");
      empty = new Image("othello/images/empty.png");
      marked = new Image("othello/images/marked.png");
      label_count_p1.setText(2+"");
      label_count_p2.setText(2+"");
      cont = 2;
      isFinita = false;
      myGrid.setDisable(true);
      cleanGrid();
      cont_p1 = 2;
      cont_p2 = 2;
      totScoreP1 = 0;
      totScoreP2 = 0;
      btn_pass.setDisable(true);
   }
   
   
   /** 
    * Method combined to "board.fxml" document, where the pawns's grid takes an input 
    * generated clicking with the mouse into next move coords.   
    */
   @FXML
   private void handleOnMouseClicked(MouseEvent event) {
      cleanMarked();
      ImageView source = (ImageView)event.getSource();
      Integer colIndex = (GridPane.getColumnIndex(source) == null) ?  0 : (GridPane.getColumnIndex(source));
      Integer rowIndex = (GridPane.getRowIndex(source) == null) ? 0 : (GridPane.getRowIndex(source));
      //System.out.println("Contatore: "+cont);
      if(cont%2 != 0){
         if(!myBoard.get((rowIndex * 8) + colIndex).getImage().equals(white) && !myBoard.get((rowIndex * 8) + colIndex).getImage().equals(black)){
            finishComplete = checkWhite(colIndex, rowIndex);
         }
         else
            label_output.setText("Posizione non valida!");
      }
      else if(cont%2 == 0){
         if(!myBoard.get((rowIndex * 8) + colIndex).getImage().equals(white) && !myBoard.get((rowIndex * 8) + colIndex).getImage().equals(black)){
            finishComplete = checkBlack(colIndex, rowIndex);
         }
         else
            label_output.setText("Posizione non valida!");
      }
      label_coord.setText((colIndex+1) + " : " + (rowIndex+1));
      contaPedine();
      finishComplete = endGame();
      if(finishComplete)
         whoWon();
   }
   
   
   /**
    * Method that checks if the white pawn can do the next move or if the game is finished. If it's finished, 
    * the method will call another method to update the ranking between the two players. 
    */
   private boolean checkWhite(int colIndex, int rowIndex) {
      isFinita = endGame();
      if(!isFinita){
         if(nextPlay(colIndex, rowIndex, white)){
            cont++;
            label_output.setText("Black's turn");
         }
         else{
            //System.out.println("White's move not avaible");
         }
      }
      return isFinita;
   }
   
   
   /**
    * Method that checks if the black pawn can do the next move or if the game is finished. If it's finished, 
    * the method will call another method to update the ranking between the two players. 
    */
   private boolean checkBlack(int colIndex, int rowIndex) {
      isFinita = endGame();
      if(!isFinita){
         if(nextPlay(colIndex, rowIndex, black)){
            cont++;
            label_output.setText("White's turn");
         }
//         else{
//            System.out.println("Black's move not avaible");
//         }
      }
      return isFinita;
   }
   
   
   /**
    * Method that notifies me if the next move is possible.
    * Method is invoked by "checkBlack" and "checkWhite".
    * Method checks if there is a legal move for the pawn in the 8 possible directions.
    */
   private boolean nextPlay(int colIndex, int rowIndex, Image image){
      boolean isOk = false;
      
       //Check the upper pawn
      if(rowIndex > 1){
         if(!myBoard.get(((rowIndex-1) * 8) + colIndex).getImage().equals(image) && !myBoard.get(((rowIndex-1) * 8) + colIndex).getImage().equals(empty)){
            //System.out.println("SOPRA");
            for(int i = rowIndex-1; i > 0; i--){
               if(myBoard.get(((i-1) * 8) + colIndex).getImage().equals(image)){
                  //System.out.println("check");
                  for(int k = rowIndex; k >= (i); k--){
                     myBoard.get((k * 8) + colIndex).setImage(image);
                  }
                  isOk = true;
                  break;
               }
               else if(myBoard.get(((i-1) * 8) + colIndex).getImage().equals(empty)){
                  //System.out.println("Fermo il ciclo, tanto e' vuota la pedina dopo");
                  break;
               }
            }
         }
      }
      
      
     //Check the pawn below
      if(rowIndex < 6){
         if(!myBoard.get(((rowIndex+1) * 8) + colIndex).getImage().equals(image) && !myBoard.get(((rowIndex+1) * 8) + colIndex).getImage().equals(empty)){
            //System.out.println("SOTTO");
            for(int i = rowIndex+1; i < 7; i++){
               if(myBoard.get(((i+1) * 8) + colIndex).getImage().equals(image)){
                  //System.out.println("check");
                  for(int k = rowIndex; k <= (i); k++){
                     myBoard.get((k * 8) + colIndex).setImage(image);
                  }
                  isOk = true;
                  break;
               }
               else if(myBoard.get(((i+1) * 8) + colIndex).getImage().equals(empty)){
                  //System.out.println("Fermo il ciclo, tanto e' vuota la pedina dopo");
                  break;
               }
            }
         }
      }
      
     //Check the right pawn
      if(colIndex < 6){
         if(!myBoard.get((rowIndex * 8) + (colIndex+1)).getImage().equals(image) && !myBoard.get((rowIndex * 8) + (colIndex+1)).getImage().equals(empty)){
            //System.out.println("DESTRA");
            for(int i = colIndex+1; i < 7; i++){
               if(myBoard.get((rowIndex * 8) + (i+1)).getImage().equals(image)){
                  //System.out.println("check");
                  for(int k = colIndex; k <= (i); k++){
                     myBoard.get((rowIndex * 8) + k).setImage(image);
                  }
                  isOk = true;
                  break;
               }
               else if(myBoard.get((rowIndex * 8) + (i+1)).getImage().equals(empty)){
                  //System.out.println("Fermo il ciclo, tanto e' vuota la pedina dopo");
                  break;
               }
            }
         }
      }
      
      //Check the left pawn
      if(colIndex > 1){
         if(!myBoard.get((rowIndex * 8) + (colIndex-1)).getImage().equals(image) && !myBoard.get((rowIndex * 8) + (colIndex-1)).getImage().equals(empty)){
            //System.out.println("SINISTRA");
            for(int i = colIndex-1; i > 0; i--){
               if(myBoard.get((rowIndex * 8) + (i-1)).getImage().equals(image)){
                  //System.out.println("check");
                  for(int k = colIndex; k >= (i); k--){
                     myBoard.get((rowIndex * 8) + k).setImage(image);
                  }
                  isOk = true;
                  break;
               }
               else if(myBoard.get((rowIndex * 8) + (i-1)).getImage().equals(empty)){
                  //System.out.println("Fermo il ciclo, tanto e' vuota la pedina dopo");
                  break;
               }
            }
         }
      }
      
      int o = rowIndex-1;
      int rowTemp = rowIndex;
      //Check the upper-left pawn
      if(colIndex > 1 && rowIndex > 1){
         if(!myBoard.get(((rowIndex-1) * 8) + (colIndex-1)).getImage().equals(image) && !myBoard.get(((rowIndex-1) * 8) + (colIndex-1)).getImage().equals(empty)){
            //System.out.println("SOPRA-SINISTRA");
            for(int i = colIndex-1; i > 0; i--){
               if(o > 0){
                  if(myBoard.get(((o-1) * 8) + (i-1)).getImage().equals(image)){
                     //System.out.println("check");
                     for(int k = colIndex; k >= (i); k--){
                        myBoard.get((rowTemp * 8) + k).setImage(image);
                        rowTemp--;
                     }
                     isOk = true;
                     break;
                  }
                  else if(myBoard.get(((o-1) * 8) + (i-1)).getImage().equals(empty)){
                     //System.out.println("Fermo il ciclo, tanto e' vuota la pedina dopo");
                     break;
                  }
                  o--;
               }
               else
                  break;
            }
         }
      }
      
      //Check the below right pawn
      o = rowIndex+1;
      rowTemp = rowIndex;
      if(rowIndex < 6 && colIndex < 6){
         if(!myBoard.get(((rowIndex+1) * 8) + (colIndex+1)).getImage().equals(image) && !myBoard.get(((rowIndex+1) * 8) + (colIndex+1)).getImage().equals(empty)){
            for(int i = colIndex+1; i < 7; i++){
               if(o < 7){
                  //System.out.println("SOTTO-DESTRA");
                  if(myBoard.get(((o+1) * 8) + (i+1)).getImage().equals(image)){
                     //System.out.println("check");
                     for(int k = colIndex; k <= (i); k++){
                        myBoard.get((rowTemp * 8) + k).setImage(image);
                        rowTemp++;
                        //System.out.println("Mangiata");
                     }
                     //System.out.println(isOk);
                     isOk = true;
                     break;
                  }
                  else if(myBoard.get(((o+1) * 8) + (i+1)).getImage().equals(empty)){
                     //System.out.println("Fermo il ciclo, tanto e' vuota la pedina dopo");
                     break;
                  }
                  o++;
               }
               else
                  break;
            }
         }
      }
      
      //Check the upper-right pawn
      o = colIndex+1;
      int colTemp = colIndex;
      if(rowIndex > 1 && colIndex < 6){
         if(!myBoard.get(((rowIndex-1) * 8) + (colIndex+1)).getImage().equals(image) && !myBoard.get(((rowIndex-1) * 8) + (colIndex+1)).getImage().equals(empty)){
            //System.out.println("SOPRA-DESTRA");
            for(int i = rowIndex-1; i > 0; i--){
               if(o < 7){
                  if(myBoard.get(((i-1) * 8) + (o+1)).getImage().equals(image)){
                     //System.out.println("check");
                     for(int k = rowIndex; k >= (i); k--){
                        myBoard.get((k * 8) + colTemp).setImage(image);
                        colTemp++;
                     }
                     isOk = true;
                     break;
                  }
                  else if(myBoard.get(((i-1) * 8) + (o+1)).getImage().equals(empty)){
                     //System.out.println("Fermo il ciclo, tanto e' vuota la pedina dopo");
                     break;
                  }
                  o++;
               }
               else
                  break;
            }
         }
      }
      
      //Check the below left pawn
      o = colIndex-1;
      rowTemp = rowIndex;
      if(rowIndex < 6 && colIndex > 1){
         if(!myBoard.get(((rowIndex+1) * 8) + (colIndex-1)).getImage().equals(image) && !myBoard.get(((rowIndex+1) * 8) + (colIndex-1)).getImage().equals(empty)){
            //System.out.println("SOTTO-SINISTRA");
            for(int i = rowIndex+1; i < 7; i++){
               if(o > 0){
                  if(myBoard.get(((i+1) * 8) + (o-1)).getImage().equals(image)){
                     //System.out.println("check");
                     for(int k = colIndex; k >= (o); k--){
                        myBoard.get((rowTemp * 8) + k).setImage(image);
                        rowTemp++;
                     }
                     isOk = true;
                     break;
                  }
                  else if(myBoard.get(((i+1) * 8) + (o-1)).getImage().equals(empty)){
                     //System.out.println("Fermo il ciclo, tanto e' vuota la pedina dopo");
                     break;
                  }
                  o--;
               }
               else
                  break;
            }
         }
      }
      return isOk;
   }
   
   
   /**
    * Method invoked by "checkWhite" and "checkBlack" to notify the players if the game is over.
    * Before that we check the game with the method "endGame()". 
    */
   private void whoWon(){
      totScoreP1 = Integer.parseInt(p1_totScore.getText().trim());
      totScoreP2 = Integer.parseInt(p2_totScore.getText().trim());
      int scoreP1 = Integer.parseInt(label_count_p1.getText().trim());
      int scoreP2 = Integer.parseInt(label_count_p2.getText().trim());
      //P1 wins
      if(scoreP1 > scoreP2){
         label_output.setText(txt_field_p1.getText()+" wins!");
         totScoreP1++;
         p1_totScore.setText(totScoreP1+"");
         myGrid.setDisable(true);
         
         //It allows me to add a string in an arraylist to print the info about the game
         if(lastPlays.size() < 10){
            lastPlays.add(txt_field_p1.getText()+" wins!"+"\n"+"--Score--"+"\n"+txt_field_p1.getText()+": "+totScoreP1+"\n"+txt_field_p2.getText()+": "+totScoreP2+"\n"+"\n");
         }
         else{
            lastPlays.remove(0);
            lastPlays.add(txt_field_p1.getText()+" wins!"+"\n"+"--Score--"+"\n"+txt_field_p1.getText()+": "+totScoreP1+"\n"+txt_field_p2.getText()+": "+totScoreP2+"\n"+"\n");
         }
      }
      //TIE
      else if(scoreP1 == scoreP2){
         label_output.setText("Pareggio!");
         myGrid.setDisable(true);
         if(lastPlays.size() < 10){
            lastPlays.add("Tie"+"\n"+"--Score--"+"\n"+txt_field_p1.getText()+": "+totScoreP1+"\n"+txt_field_p2.getText()+": "+totScoreP2+"\n"+"\n");
         }
         else{
            lastPlays.remove(0);
            lastPlays.add("Tie"+"\n"+"--Score--"+"\n"+txt_field_p1.getText()+": "+totScoreP1+"\n"+txt_field_p2.getText()+": "+totScoreP2+"\n"+"\n");
         }
      }
      
      //P2 wins
      else{
         label_output.setText(txt_field_p2.getText()+" wins!");
         totScoreP2++;
         p2_totScore.setText(totScoreP2+"");
         myGrid.setDisable(true);
         if(lastPlays.size() < 10){
            lastPlays.add(txt_field_p2.getText()+" wins!"+"\n"+"--Score--"+"\n"+txt_field_p1.getText()+": "+totScoreP1+"\n"+txt_field_p2.getText()+": "+totScoreP2+"\n"+"\n");
         }
         else{
            lastPlays.remove(0);
            lastPlays.add(txt_field_p2.getText()+" wins!"+"\n"+"--Score--"+"\n"+txt_field_p1.getText()+": "+totScoreP1+"\n"+txt_field_p2.getText()+": "+totScoreP2+"\n"+"\n");
         }
      }
   }
   
   
   /**
    * Method invoked by "checkWhite" and "checkBlack" to check if the game is over.
    * If yes, the method will return a boolean "true".
    */
   private boolean endGame() {
      boolean finePartita = false;
      int contGame = 0;
      for(int i = 0; i < 8; i++){
         for(int j = 0; j < 8; j++){
            if(myBoard.get((i * 8) + j).getImage().equals(black) || myBoard.get((i * 8) + j).getImage().equals(white)){
               contGame++;
               if (contGame >= 64){
                  finePartita = true;
                  break;
               }
               else
                  finePartita = false;
            }
            else if(Integer.parseInt(label_count_p1.getText()) <= 0 || Integer.parseInt(label_count_p2.getText()) <= 0){
               finePartita = true;
               break;
            }
            
         }
         if(finePartita)
            break;
      }
      //System.out.println("Partita finita?1 "+finePartita);
      return finePartita;
   }
   
   /**
    * This Method ends the game execution.
    */
   @FXML
   private void exitProgram(ActionEvent event) {
      System.exit(0);
   }
   
   
   
   
   /**
    * This Method resets the game of the two players, keeping only the names of the two players and their overall score.
    */
   @FXML
   private void restartGame(ActionEvent event)throws Exception{
      cleanGrid();
      rd_p1_white.setSelected(false);
      rd_p2_white.setSelected(false);
      rd_p1_black.setSelected(false);
      rd_p2_black.setSelected(false);
      btn_start.setDisable(false);
      btn_pass.setDisable(true);
      rd_p1_black.setDisable(false);
      rd_p2_black.setDisable(false);
      rd_p1_white.setDisable(false);
      rd_p2_white.setDisable(false);
      myGrid.setDisable(true);
      label_output.setText("Turno del nero");
   }
   
   
   /**
    * This method is used to open the game's rules opening a new scene.
    */
   @FXML
   public void playRules2(ActionEvent event) throws IOException {
      Stage primaryStage = new Stage();
      Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("othello/view/instruction.fxml"));
      Scene scene = new Scene(root);
      scene.getStylesheets().add("othello/view/lightTheme.css");
      
      primaryStage.setScene(scene);
      primaryStage.setResizable(false);
      primaryStage.sizeToScene();
      primaryStage.setTitle("Instruction");
      primaryStage.show();
   }
   
   
   /**
    * This method is to start a new game.
    * It will check if the inserted  data are correct.
    */
   @FXML
   private void newGame(ActionEvent event) throws Exception {
      if(txt_field_p1.getText().trim().equalsIgnoreCase("") || txt_field_p2.getText().equalsIgnoreCase("")){
         alert = new Alert(Alert.AlertType.INFORMATION);
         alert.setHeaderText("Information!");
         alert.setTitle("Error: Wrong player's name");
         alert.setContentText("Insert a valid player's name!");
         alert.showAndWait();
      }
      
      else if(txt_field_p1.getText().trim().equalsIgnoreCase(txt_field_p2.getText().trim())){
         alert = new Alert(Alert.AlertType.INFORMATION);
         alert.setHeaderText("Information!");
         alert.setTitle("Error: Wrong, same player's name");
         alert.setContentText("You cannot use the same player's name.");
         alert.showAndWait();
      }
      
      else if(rd_p1_black.isSelected() == false && rd_p1_white.isSelected() == false){
         alert = new Alert(Alert.AlertType.INFORMATION);
         alert.setHeaderText("Information!");
         alert.setTitle("Error: Choose color");
         alert.setContentText("You have to choose a color, before playing.");
         alert.showAndWait();
      }
      else{
         myGrid.setDisable(false);
         txt_field_p1.setEditable(false);
         txt_field_p2.setEditable(false);
         btn_start.setDisable(true);
         btn_pass.setDisable(false);
         rd_p1_black.setDisable(true);
         rd_p2_black.setDisable(true);
         rd_p1_white.setDisable(true);
         rd_p2_white.setDisable(true);
      }
   }
   
   
   /**
    * This method is about choosing the pawn's color between the players. 
    * Everytime the game is resetted you can change the color.
    */
   @FXML
   private void chooseColor(ActionEvent event) {
      if(rd_p1_black.isArmed() == true || rd_p2_white.isArmed() == true){
         rd_p1_black.setSelected(true);
         rd_p1_white.setSelected(false);
         rd_p2_black.setSelected(false);
         rd_p2_white.setSelected(true);
         imgCount_p1.setImage(black);
         imgCount_p2.setImage(white);
      }
      else if(rd_p2_black.isArmed() == true || rd_p1_white.isArmed() == true){
         rd_p1_black.setSelected(false);
         rd_p1_white.setSelected(true);
         rd_p2_black.setSelected(true);
         rd_p2_white.setSelected(false);
         imgCount_p2.setImage(black);
         imgCount_p1.setImage(white);
      }
   }
   
   
   /**
    * This method cleans the board and initialize it with 4 pawn in the middle.
    */
   private void cleanGrid() {
      //myGrid.getChildren().removeIf(ImageView.class::isInstance);
      cont = 2;
      for (int i = 0; i < 8; i++){ //Per righe
         for (int j = 0; j < 8; j++){ // Per colonne
            myBoard.get((i*8)+j).setImage(empty);
         }
      }
      myBoard.get((3*8)+3).setImage(black);
      myBoard.get((4*8)+4).setImage(black);
      myBoard.get((3*8)+4).setImage(white);
      myBoard.get((4*8)+3).setImage(white);
      label_count_p1.setText(2+"");
      label_count_p2.setText(2+"");
      cont_p1 = 2;
      cont_p2 = 2;
   }
   
   private void cleanMarked() {
      for (int i = 0; i < 8; i++){ //Per righe
         for (int j = 0; j < 8; j++){ // Per colonne
            if(myBoard.get((i * 8) + j).getImage().equals(marked)){
               myBoard.get((i * 8) + j).setImage(empty);
            }
         }
      }
   }
   
   /**
    * This is a manual method, initialized at the program start, to create a vector of "ImageView" objects.
    */
   private void createMyBoard() {
      myBoard = new ArrayList<>();
      myBoard.add(imageView00);
      myBoard.add(imageView01);
      myBoard.add(imageView02);
      myBoard.add(imageView03);
      myBoard.add(imageView04);
      myBoard.add(imageView05);
      myBoard.add(imageView06);
      myBoard.add(imageView07);
      myBoard.add(imageView10);
      myBoard.add(imageView11);
      myBoard.add(imageView12);
      myBoard.add(imageView13);
      myBoard.add(imageView14);
      myBoard.add(imageView15);
      myBoard.add(imageView16);
      myBoard.add(imageView17);
      myBoard.add(imageView20);
      myBoard.add(imageView21);
      myBoard.add(imageView22);
      myBoard.add(imageView23);
      myBoard.add(imageView24);
      myBoard.add(imageView25);
      myBoard.add(imageView26);
      myBoard.add(imageView27);
      myBoard.add(imageView30);
      myBoard.add(imageView31);
      myBoard.add(imageView32);
      myBoard.add(imageView33);
      myBoard.add(imageView34);
      myBoard.add(imageView35);
      myBoard.add(imageView36);
      myBoard.add(imageView37);
      myBoard.add(imageView40);
      myBoard.add(imageView41);
      myBoard.add(imageView42);
      myBoard.add(imageView43);
      myBoard.add(imageView44);
      myBoard.add(imageView45);
      myBoard.add(imageView46);
      myBoard.add(imageView47);
      myBoard.add(imageView50);
      myBoard.add(imageView51);
      myBoard.add(imageView52);
      myBoard.add(imageView53);
      myBoard.add(imageView54);
      myBoard.add(imageView55);
      myBoard.add(imageView56);
      myBoard.add(imageView57);
      myBoard.add(imageView60);
      myBoard.add(imageView61);
      myBoard.add(imageView62);
      myBoard.add(imageView63);
      myBoard.add(imageView64);
      myBoard.add(imageView65);
      myBoard.add(imageView66);
      myBoard.add(imageView67);
      myBoard.add(imageView70);
      myBoard.add(imageView71);
      myBoard.add(imageView72);
      myBoard.add(imageView73);
      myBoard.add(imageView74);
      myBoard.add(imageView75);
      myBoard.add(imageView76);
      myBoard.add(imageView77);
   }
   
   
   
   /**
    * This method returns the players score counting the in-game pawns.
    */
   private void contaPedine() {
      int[] punteggio = new int[2];
      for (int i = 0; i < 8; i++){ //Per righe
         for (int j = 0; j < 8; j++){ // Per colonne
            if(myBoard.get((i*8)+j).getImage().equals(black))
               punteggio[0]++;
            if(myBoard.get((i*8)+j).getImage().equals(white))
               punteggio[1]++;
         }
         if(imgCount_p1.getImage().equals(black)){
            label_count_p1.setText(punteggio[0]+"");
            label_count_p2.setText(punteggio[1]+"");
         }
         else{
            label_count_p1.setText(punteggio[1]+"");
            label_count_p2.setText(punteggio[0]+"");
         }
      }
   }
   
   
   /**
    * This method initialize all over. Including players names and the overall scores.
    */
   @FXML
   private void cleanStart(ActionEvent event) {
      cleanGrid();
      rd_p1_white.setSelected(false);
      rd_p2_white.setSelected(false);
      rd_p1_black.setSelected(false);
      rd_p2_black.setSelected(false);
      txt_field_p1.setText("");
      txt_field_p2.setText("");
      txt_field_p1.setEditable(true);
      txt_field_p2.setEditable(true);
      btn_start.setDisable(false);
      btn_pass.setDisable(true);
      rd_p1_black.setDisable(false);
      rd_p2_black.setDisable(false);
      rd_p1_white.setDisable(false);
      rd_p2_white.setDisable(false);
      myGrid.setDisable(true);
      label_output.setText("Ricominciamo!");
      p1_totScore.setText("0");
      p2_totScore.setText("0");
   }
   
   @FXML
   private void openLog(ActionEvent event) throws FileNotFoundException, IOException {
      if(txt_field_p1.getText().trim().equalsIgnoreCase("") || txt_field_p2.getText().equalsIgnoreCase("")){
         //System.out.println("Nome giocatore errato!");
         alert = new Alert(Alert.AlertType.INFORMATION);
         alert.setHeaderText("Information!");
         alert.setTitle("Error: Wrong player's name");
         alert.setContentText("Insert a valid player's name!");
         alert.showAndWait();
      }
      
      else if(txt_field_p1.getText().trim().equalsIgnoreCase(txt_field_p2.getText().trim())){
         //System.out.println("Non usare due nomi uguali!");
         alert = new Alert(Alert.AlertType.INFORMATION);
         alert.setHeaderText("Information!");
         alert.setTitle("Error: Wrong, same player's name");
         alert.setContentText("You cannot use the same player's name.");
         alert.showAndWait();
      }
      
      else if(rd_p1_black.isSelected() == false && rd_p1_white.isSelected() == false){
         //System.out.println("Prima di iniziare scegli il colore della pedina!");
         alert = new Alert(Alert.AlertType.INFORMATION);
         alert.setHeaderText("Information!");
         alert.setTitle("Error: Choose color");
         alert.setContentText("You have to choose a color, before playing.");
         alert.showAndWait();
      }
      else{
         if(p1_totScore.getText().equals("0") && p2_totScore.getText().equals("0")){
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Information!");
            alert.setTitle("Error: No matches played yet");
            alert.setContentText("Wait at least 1 match played to use the function");
            alert.showAndWait();
         }
         else{
            FileOutputStream fileLog = null;
            File file = null;
            PrintWriter printOnFile = null;
            try{
               file = new File("fileLog.txt"); //src/othello/model/
               fileLog = new FileOutputStream(file);
            }
            catch(FileNotFoundException e){
               e.printStackTrace();
               System.out.println("File non trovato");
               System.exit(0);
            }
            finally{
               printOnFile = new PrintWriter(fileLog);
               for(int i = 0; i < lastPlays.size(); i++){
                  printOnFile.println(lastPlays.get(i));
               }
               printOnFile.close();
               Stage primaryStage = new Stage();
               Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("othello/view/logMatch.fxml"));
               Scene scene = new Scene(root);
               
               primaryStage.setScene(scene);
               primaryStage.setResizable(false);
               primaryStage.sizeToScene();
               primaryStage.setTitle("Last 10 Matches");
               primaryStage.show();
            }
         }
      }
   }
   
   @FXML
   private void helpMe(ActionEvent event) {
      cleanMarked();
      if(btn_start.isDisabled()){
         for (int i = 0; i < 8; i++){ //Per righe
            for (int j = 0; j < 8; j++){ // Per colonne
               //System.out.println(i + "  TOTALE  " + j);
               if(cont%2 != 0){
                  if(myBoard.get((i * 8) + j).getImage().equals(white)){
                     //System.out.println(i + "  " + j + " trovato bianco");
                     nextMove(j, i, white);
                     if(myBoard.get((i * 8) + j).getImage().equals(marked)){
                     }
                  }
               }
               else if(cont%2 == 0){
                  if(myBoard.get((i * 8) + j ).getImage().equals(black)){
                     //System.out.println(i + "  " + j + " trovato nero");
                     nextMove(j, i, black);
                  }
               }
            }
         }
      }
      else{
         alert = new Alert(Alert.AlertType.INFORMATION);
         alert.setHeaderText("Information!");
         alert.setTitle("Start game before");
         alert.setContentText("You have to click the start button before!");
         alert.showAndWait();
      }
   }
   
   
   private void nextMove(int colIndex, int rowIndex, Image image){
      Image imageTemp;
      if(image.equals(black)){
         imageTemp = white;
      }
      else
         imageTemp = black;
      
      //System.out.println("Checkkiamo");
      //Check upper pawn
      if(rowIndex > 1){
         //System.out.println("entro su");
         if(myBoard.get(((rowIndex-1) * 8) + colIndex).getImage().equals(imageTemp)){
            //System.out.println("Controllo su, riga: "+ rowIndex + " colonna: "+ colIndex);
            for(int i = rowIndex-1; i > 0; i--){
               if(myBoard.get(((i-1) * 8) + colIndex).getImage().equals(empty) || myBoard.get(((i-1) * 8) + colIndex).getImage().equals(marked)){
                  //System.out.println("Coloro "+(i-1)+" "+colIndex);
                  myBoard.get(((i-1) * 8) + colIndex).setImage(marked);
                  break;
               }
               else if(myBoard.get(((i-1) * 8) + colIndex).getImage().equals(image)){
                  //System.out.println("Passo!!!!");
                  break;
               }
            }
         }
      }
      
      
      //Check below pawn
      if(rowIndex < 6){
         if(myBoard.get(((rowIndex+1) * 8) + colIndex).getImage().equals(imageTemp)){
            //System.out.println("Controllo giu, riga: "+ rowIndex + " colonna: "+ colIndex);
            for(int i = rowIndex+1; i < 7; i++){
               if(myBoard.get(((i+1) * 8) + colIndex).getImage().equals(empty) || myBoard.get(((i+1) * 8) + colIndex).getImage().equals(marked)){
                  //System.out.println("Coloro "+(i+1)+" "+colIndex);
                  myBoard.get(((i+1) * 8) + colIndex).setImage(marked);
                  break;
               }
               else if(myBoard.get(((i+1) * 8) + colIndex).getImage().equals(image)){
                  //System.out.println("Passo!!!!");
                  break;
               }
            }
         }
      }
      
      //Check right pawn
      if(colIndex < 6){
         //System.out.println("Entro a destra");
         if(myBoard.get((rowIndex * 8) + (colIndex+1)).getImage().equals(imageTemp)){
            //System.out.println("Controllo dx, riga: "+ rowIndex + " colonna: "+ colIndex);
            for(int i = colIndex+1; i < 7; i++){
               //System.out.println("i (colonna attuale): " + i);
               if(myBoard.get((rowIndex * 8) + (i+1)).getImage().equals(empty) || myBoard.get((rowIndex * 8) + (i+1)).getImage().equals(marked)){
                  //System.out.println("Coloro "+(rowIndex)+" "+(i+1));
                  myBoard.get((rowIndex * 8) + (i+1)).setImage(marked);
                  break;
               }
               else if(myBoard.get((rowIndex * 8) + (i+1)).getImage().equals(image)){
                  //System.out.println("Passo!!!!");
                  break;
               }
            }
         }
      }
      
      //Check left pawn
      if(colIndex > 1){
         if(myBoard.get((rowIndex * 8) + (colIndex-1)).getImage().equals(imageTemp)){
            //System.out.println("Controllo sx, riga: "+ rowIndex + " colonna: "+ colIndex);
            for(int i = colIndex-1; i > 0; i--){
               //System.out.println("Il cont attuale i e': " + i);
               if(myBoard.get((rowIndex * 8) + (i-1)).getImage().equals(empty) || myBoard.get((rowIndex * 8) + (i-1)).getImage().equals(marked)){
                  //System.out.println("Coloro "+(rowIndex)+" "+(i-1));
                  myBoard.get((rowIndex * 8) + (i-1)).setImage(marked);
                  break;
               }
               else if(myBoard.get((rowIndex * 8) + (i-1)).getImage().equals(image)){
                  //System.out.println("Passo!!!!");
                  break;
               }
            }
         }
      }
      
      
      int o = rowIndex-1;
      //Check upper-left pawn
      if(colIndex > 1 && rowIndex > 1){
         if(myBoard.get(((rowIndex-1) * 8) + (colIndex-1)).getImage().equals(imageTemp)){
            //System.out.println("Controllo sopra sx, riga: "+ rowIndex + " colonna: "+ colIndex);
            for(int i = colIndex-1; i > 0; i--){
               if(o > 0){
                  if(myBoard.get(((o-1) * 8) + (i-1)).getImage().equals(empty) || myBoard.get(((o-1) * 8) + (i-1)).getImage().equals(marked)){
                     //System.out.println("Coloro "+(o-1)+" "+(i-1));
                     myBoard.get(((o-1) * 8) + (i-1)).setImage(marked);
                     break;
                  }
                  else if(myBoard.get(((o-1) * 8) + (i-1)).getImage().equals(image)){
                     //System.out.println("Passo!!!!");
                     break;
                  }
                  o--;
               }
               else
                  break;
            }
         }
      }
      
      //Check lower-right pawn
      o = rowIndex+1;
      if(rowIndex < 6 && colIndex < 6){
         if(myBoard.get(((rowIndex+1) * 8) + (colIndex+1)).getImage().equals(imageTemp)){
            //System.out.println("Controllo sotto dx, riga: "+ rowIndex + " colonna: "+ colIndex);
            for(int i = colIndex+1; i < 7; i++){
               //System.out.println("Il contatore o e': " + o);
               if(o < 7){
                  if(myBoard.get(((o+1) * 8) + (i+1)).getImage().equals(empty) || myBoard.get(((o+1) * 8) + (i+1)).getImage().equals(marked)){
                     //System.out.println("Coloro "+(o+1)+" "+(i+1));
                     myBoard.get(((o+1) * 8) + (i+1)).setImage(marked);
                     break;
                  }
                  else if(myBoard.get(((o+1) * 8) + (i+1)).getImage().equals(image)){
                     //System.out.println("Passo!!!!");
                     break;
                  }
                  o++;
               }
               else
                  break;
            }
         }
      }
      
      //Check upper-right pawn
      o = colIndex+1;
      if(rowIndex > 1 && colIndex < 6){
         if(myBoard.get(((rowIndex-1) * 8) + (colIndex+1)).getImage().equals(imageTemp)){
            //System.out.println("Controllo sopra dx, riga: "+ rowIndex + " colonna: "+ colIndex);
            for(int i = rowIndex-1; i > 0; i--){
               if(o < 7){
                  if(myBoard.get(((i-1) * 8) + (o+1)).getImage().equals(empty) || myBoard.get(((i-1) * 8) + (o+1)).getImage().equals(marked)){
                     //System.out.println("Coloro "+(i-1)+" "+(o+1));
                     myBoard.get(((i-1) * 8) + (o+1)).setImage(marked);
                     break;
                  }
                  else if(myBoard.get(((i-1) * 8) + (o+1)).getImage().equals(image)){
                     //System.out.println("Passo!!!!");
                     break;
                  }
                  o++;
               }
               else
                  break;
            }
         }
      }
      
      //Check lower-left pawn
      o = colIndex-1;
      if(rowIndex < 6 && colIndex > 1){
         if(myBoard.get(((rowIndex+1) * 8) + (colIndex-1)).getImage().equals(imageTemp)){
            //System.out.println("Controllo sotto sx, riga: "+ rowIndex + " colonna: "+ colIndex);
            for(int i = rowIndex+1; i < 7; i++){
               if(o > 0){
                  //System.out.println("Il cont attuale i e': " + i + " mentre l' o e': " + o);
                  if(myBoard.get(((i+1) * 8) + (o-1)).getImage().equals(empty) || myBoard.get(((i+1) * 8) + (o-1)).getImage().equals(marked)){
                     //System.out.println("Coloro "+(i+1)+" "+(o-1));
                     myBoard.get(((i+1) * 8) + (o-1)).setImage(marked);
                     break;
                  }
                  else if(myBoard.get(((i+1) * 8) + (o-1)).getImage().equals(image)){
                     //System.out.println("Passo!!!!");
                     break;
                  }
                  o--;
               }
               else
                  break;
            }
         }
      }
   }
   
   @FXML
   private void passRound(ActionEvent event) {
      boolean found = false;
      if(btn_start.isDisabled()){
         cleanMarked();
         for (int i = 0; i < 8; i++){ //Per righe
            for (int j = 0; j < 8; j++){ // Per colonne
               if(cont%2 != 0){
                  if(myBoard.get((i * 8) + j).getImage().equals(white)){
                     nextMove(j, i, white);
                  }
               }
               else if(cont%2 == 0){
                  if(myBoard.get((i * 8) + j ).getImage().equals(black)){
                     nextMove(j, i, black);
                  }
               }
            }
         }
         for (int l = 0; l < 8; l++){ //Per righe
            for (int k = 0; k < 8; k++){ // Per colonne
               if(myBoard.get((l * 8) + k).getImage().equals(marked)){
                  alert = new Alert(Alert.AlertType.INFORMATION);
                  alert.setHeaderText("Information!");
                  alert.setTitle("Move avaible");
                  alert.setContentText("You cannot pass the turn.");
                  alert.showAndWait();
                  found = true;
                  break;
               }
            }
            if(found)
               break;
         }
         if(!found){
            cont++;
            if(cont%2 != 0){
               label_output.setText("Black's turn");
            }
            else{
               label_output.setText("White's turn");
            }
         }
      }
      else{
         alert = new Alert(Alert.AlertType.INFORMATION);
         alert.setHeaderText("Information!");
         alert.setTitle("Start game before");
         alert.setContentText("You have to click the start button before!");
         alert.showAndWait();
      }
   }
   
   
   /**
   	* This method displays "about" from the item menu
   	*/
   @FXML
   public void showAbout(ActionEvent event) throws IOException {
      Stage primaryStage = new Stage();
      Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("othello/view/about.fxml"));
      Scene scene = new Scene(root);
      scene.getStylesheets().add("othello/view/about.css");
      
      primaryStage.setScene(scene);
      primaryStage.setResizable(false);
      primaryStage.sizeToScene();
      primaryStage.setTitle("About");
      primaryStage.show();
   }
   
   
   
   /**
	 * This method displays team's background from the item menu in HTML format 
	 */
   @FXML
   public void showBackground(ActionEvent event) throws IOException {
      Stage primaryStage = new Stage();
      Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("othello/view/teamBackground.fxml"));
      Scene scene = new Scene(root);
      scene.getStylesheets().add("othello/view/teamBackground.css");
      
      primaryStage.setScene(scene);
      primaryStage.setResizable(false);
      primaryStage.sizeToScene();
      primaryStage.setTitle("Team Background");
      primaryStage.show();
   }
}