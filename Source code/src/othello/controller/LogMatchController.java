/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package othello.controller;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;

/**
 *  This class displays the last 10 games played.
 *
 *  @author Alberto Zini
 *  @author Giacomo Rondelli 
 *  @author Giacomo Pazzaglia
 *  @version 1.0
 *  @since 2017.08.01
 */
public class LogMatchController implements Initializable {
   
   @FXML
   private TextArea txtArea;
   private static final Logger LOG = Logger.getLogger(LogMatchController.class.getName());
   private Future<List<String>> future;
   private ExecutorService executorService = Executors.newSingleThreadExecutor();
   private TextFileReader reader = new TextFileReader();
   
   /**
    * Initializes the controller class.
    */
   @Override
   public void initialize(URL url, ResourceBundle rb) {
      
   }
   
   
   @FXML
   @SuppressWarnings("NestedAssignment")
   public void showFileLines() throws InterruptedException, ExecutionException {
      future = executorService.submit(new Callable<List<String>>() {
         public List<String> call() throws Exception {
            return reader.read(new File("fileLog.txt"));
         }
      });
      List<String> lines = future.get();
      executorService.shutdownNow();
      txtArea.clear();
      for (String line : lines ) {
         txtArea.appendText(line + "\n");
      }
   }
}
