/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package othello.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuBar;
import javafx.scene.media.AudioClip;
import static javafx.scene.media.AudioClip.INDEFINITE;

/**
 * This class allows us to execute the game, change a different theme (light or dark) and choose the corresponding moves in the challenge between the players.
 *  @author Alberto Zini
 *  @author Giacomo Rondelli 
 *  @author Giacomo Pazzaglia
 *  @version 1.0
 *  @since 2017.08.01
 */
public class StartImageDarkController implements Initializable {
   
   @FXML
   private MenuBar myMenuBar;
   private AudioClip audio;
   private AudioClip beep;
   
   /**
    * Initializes the controller class.
    */
   @Override
   public void initialize(URL url, ResourceBundle rb) {
      audio = new AudioClip(getClass().getClassLoader().getResource("othello/music/darktheme.wav").toExternalForm());
      beep = new AudioClip(getClass().getClassLoader().getResource("othello/music/button.wav").toExternalForm());
      audio.stop();
      audio.setVolume(0.2);
      audio.setCycleCount(INDEFINITE);
      audio.play();
   }
   
   @FXML
   public void lightTheme(ActionEvent event) throws IOException {
      //Stop the light theme song and start the dark theme song
      audio.stop();
      Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("othello/view/startImage.fxml"));
      Scene scene = new Scene(root);
      Stage stage = (Stage) myMenuBar.getScene().getWindow();
      stage.setScene(scene);
      stage.show();
      
   }
   
   /**
    * This method start the music form the menu item
    */
   @FXML
   public void audioY(ActionEvent e)throws IOException{
      audio.stop();
      audio.setVolume(0.2);
      audio.setCycleCount(INDEFINITE);
      audio.play();
   }
   
   /**
    * Method to disable music from the menu item
    */
   @FXML
   public void audioN(ActionEvent e)throws IOException{
      audio.stop();
   }
   
   
   /**   
   	* This method quits the whole program
    */
   @FXML
   private void exitProgram(ActionEvent event) {
      System.exit(0);
   }
   
   /**
   	* This method quits only the stage.
   	*/
   private void exitStage(ActionEvent event){
      ((Node)(event.getSource())).getScene().getWindow().hide();
   }
   
   
   /**
    * Method to start the game session.
    */
   @FXML
   public void startGame(ActionEvent event) throws IOException {
      audio.stop();
      beep.play();
      audio.play();
      Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("othello/view/board.fxml"));
      Scene scene = new Scene(root);
      scene.getStylesheets().add("othello/view/darkTheme.css");
      
      //The scene enters in fade
      FadeTransition ft = new FadeTransition(Duration.millis(3000), root);
      ft.setFromValue(0.0);
      ft.setToValue(1.0);
      ft.play();
      
      Node node=(Node) event.getSource();
      Stage stage=(Stage) node.getScene().getWindow();
      stage.setResizable(false);
      stage.setTitle("Othello Dark - Game session");
      stage.setScene(scene);
      stage.show();
   }
   
   
   /** 
    * This method displays the instructions from the menu item
    */
   @FXML
   public void showInstruction(ActionEvent event) throws IOException {
      audio.stop();
      audio.play();
      Stage primaryStage = new Stage();
      Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("othello/view/instructionDark.fxml"));
      Scene scene = new Scene(root);
      
      primaryStage.setScene(scene);
      primaryStage.setResizable(false);
      primaryStage.sizeToScene();
      primaryStage.setTitle("Instruction");
      primaryStage.show();
   }
   
   
   /**
    * This Method displays "about" from the menu item
    */
   @FXML
   public void showAbout(ActionEvent event) throws IOException {
      Stage primaryStage = new Stage();
      Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("othello/view/about.fxml"));
      Scene scene = new Scene(root);
      scene.getStylesheets().add("othello/view/aboutDark.css");
      
      primaryStage.setScene(scene);
      primaryStage.setResizable(false);
      primaryStage.sizeToScene();
      primaryStage.setTitle("About");
      primaryStage.show();
   }
   
   /**
    * This method displays team's background from the item menu in HTML format
    */
   @FXML
   public void showBackground(ActionEvent event) throws IOException {
      Stage primaryStage = new Stage();
      Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("othello/view/teamBackground.fxml"));
      Scene scene = new Scene(root);
      scene.getStylesheets().add("othello/view/teamBackgroundDark.css");
      
      primaryStage.setScene(scene);
      primaryStage.setResizable(false);
      primaryStage.sizeToScene();
      primaryStage.setTitle("Team Background");
      primaryStage.show();
   }
}